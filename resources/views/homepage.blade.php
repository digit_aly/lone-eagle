<!DOCTYPE html>
<html>
<head>
	<title>Pre-built, high quality escape games for sale</title>
	<meta charset="utf-8">
	 
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://use.fontawesome.com/e81ce16d2e.js"></script>	
	<link href="/css/public.css" rel="stylesheet">
	<script type="text/javascript" src="/js/public.js"></script>
</head>
<body>
	<div class="row">
		<div class="bg">
			<div class="container">
				<div class="row">	
					<h1 class="title-headline col-sm-8 col-sm-offset-2">We build high quality, immersive and modular escape games</h1>
				</div>
				<div class="row">
					<h4 class="subheading">From the team behind <a class="link" href="canyouescape.co.uk">Can You Escape?</a></h4>
				</div>
				<div class="row icons">
					<div class="col-sm-3 hidden-xs" style="text-align: center;">
						<span class="fa-stack fa-large">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-key fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="col-sm-3 hidden-xs" style="text-align: center;">
						<span class="fa-stack fa-large">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-puzzle-piece fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="col-sm-3 hidden-xs" style="text-align: center;">
						<span class="fa-stack fa-large">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-map-o fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="col-sm-3 hidden-xs" style="text-align: center;">
						<span class="fa-stack fa-large">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-truck fa-stack-1x fa-inverse"></i>
						</span>
					</div>
				</div>
				<div class="row">
					<div class="hidden-md hidden-lg hidden-sm col-xs-12" style="text-align: center;">
						<span class="fa-stack fa-3x">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-key fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="col-sm-3 col-xs-12" style="text-align: center;">
						<h3>Turn-key escape game solution</h3>
						<p>All of our games will be designed and built off-site and delivered directly to your site by our team who will then install the modules in situ.  This minimises distruption to your day to day operation and means you can start running games right away.</p>
					</div>
					<div class="hidden-md hidden-lg hidden-sm col-xs-12" style="text-align: center;">
						<span class="fa-stack fa-3x">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-puzzle-piece fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="col-sm-3 col-xs-12" style="text-align: center;">
						<h3>Custom built, high quality puzzles</h3>
						<p>We take great pride in developing custom, innovative and high quality props.  Our team is made up of set designers, prop makers and micro electronics specialists to make sure what we build impresses customers, is robust and delivers an immersive experience.</p>
					</div>
					<div class="hidden-md hidden-lg hidden-sm col-xs-12" style="text-align: center;">
						<span class="fa-stack fa-3x">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-map-o fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="col-sm-3 col-xs-12" style="text-align: center;">
						<h3>Full documentation provided</h3>
						<p>All of the games that we provide will come with a full set of instructions that will explain how everything works, how the game runs, how to reset the room quickly and backup solutions in the unlikely event of something going wrong.</p>
					</div>
					<div class="hidden-md hidden-lg hidden-sm col-xs-12" style="text-align: center;">
						<span class="fa-stack fa-3x">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-truck fa-stack-1x fa-inverse"></i>
						</span>
					</div>
					<div class="col-sm-3 col-xs-12" style="text-align: center;">
						<h3>Fully portable designs available</h3>
						<p>We're excited to be able offer fully portable escape game solutions.  Our first portable room is Operation Blue November which can be transported in a short wheel base van and constructed in 2 hours in any room big enough.</p>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="bg-2">
			<div class="container" id="service">
				<h1>Our Service</h1>
				<div class="col-sm-6">
					<p>As a team, we've been designing, building and running escape games for 3 years now and we our has always been on making fun, immersive and interactive rooms.  We belive that the best way to achieve this is by using a modular system that allows for greater interactivity between puzzles, without the headache on concealing everything that goes on behind the scenes.</p>

					<p>We will work with the space that you have on site to design and build a modular game that will be tested off-site, delivered and installed by our team and provide you with a stress free way to launch new games at your venue.</p>
				</div>
				<div class="col-sm-6">
					<p>We have off the shelf games that such as Operation Blue November or if you have a particular theme you in mind then we can design a game to fit.</p>
					<p>All of our projects will be costed on a fixed price basis so you can be certain there won't be any hidden costs when it comes to installing the game and we'll provide ongoing support to your an your team if needed.</p>
				</div>
			</div>
		</div>
	</div>
{{-- <div class="container">
	<div class="row">
		<div>
	  	<a href="/images/067.jpg">
		    <figure>
		      <img src="/images/067.jpg" alt="">
		    </figure>
  		</a>
		</div>
	</div>
</div> --}}
<div class="row">
	<div class="container">
	<h1>Some of our work</h1>
  <div class="row">
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/rsz_008.jpg" class="thumbnail img-responsive">
        </a>
    </div>
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/rsz_015.jpg" class="thumbnail img-responsive">
        </a>
    </div>
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/rsz_021.jpg" class="thumbnail img-responsive">
        </a>
    </div>
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/rsz_067.jpg" class="thumbnail img-responsive">
        </a>
    </div>
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/rsz_078.jpg" class="thumbnail img-responsive">
        </a>
    </div>
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/rsz_049.jpg" class="thumbnail img-responsive">
        </a>
    </div>
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/045.jpg" class="thumbnail img-responsive">
        </a>
    </div>
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/051.jpg" class="thumbnail img-responsive">
        </a>
    </div>
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/043.jpg" class="thumbnail img-responsive">
        </a>
    </div>
	<div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/017.jpg" class="thumbnail img-responsive">
        </a>
    </div>
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/023.jpg" class="thumbnail img-responsive">
        </a>
    </div>
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="#">
             <img src="/images/028.jpg" class="thumbnail img-responsive">
        </a>
    </div>
  </div>
  <hr>
  </div>
</div>

<div class="row">
        <div class="videoWrapper">
            <iframe src="https://player.vimeo.com/video/184728681?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
</div>

</body>
</html>